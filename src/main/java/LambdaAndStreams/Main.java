package LambdaAndStreams;

import java.util.*;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
        List<String> imiona = Arrays.asList("Krzysiek", "Łukasz", "Anna", "Aleksandra",
                "Miłosz", "Katarzyna", "Marek", "Łucja");

        Scanner scanner = new Scanner(System.in);
        System.out.println("Wpisz pierwszą literę: ");
        String inputLine = scanner.nextLine();

        List<String> foundList = new LinkedList<>();

        for (String s : imiona) {
            if(s.startsWith(inputLine.split("")[0].toUpperCase())) foundList.add(s);
        }

        List<String> filtered;

        filtered = imiona
                .stream()
                .filter(n -> n.startsWith(inputLine.toUpperCase()))
                .collect(Collectors.toList());

        System.out.println("Znaleziono: " + foundList);
        System.out.println("Przefiltrowano: " + filtered);
    }
}
