package Zadanie1;

import lombok.Setter;

public class MySuperService {
    private IPrinter printer;

    public MySuperService(IPrinter printer) {
        this.printer = printer;
    }

    public void printData(String text){
        printer.printMessage(text);
    }
}
