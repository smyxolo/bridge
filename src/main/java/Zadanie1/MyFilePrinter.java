package Zadanie1;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class MyFilePrinter implements IPrinter {
    File file;
    PrintWriter pw;

    public MyFilePrinter() {
        this.file = new File("/Users/marekdybusc/IdeaProjects/Programowanie2Amen/Bridge/src/main/java/Zadanie1/printedText.txt");
        try {
            this.pw = new PrintWriter(new FileWriter(file, true));
        } catch (IOException e) {
            System.out.println("File not found.");
        }
    }

    @Override
    public void printMessage(String text) {
        pw.println(text);
        pw.flush();
    }
}
