package Zadanie2;

public interface IVehicle {
    void moveLeft();
    void moveRight();
    void moveUp();
    void moveDown();
}
