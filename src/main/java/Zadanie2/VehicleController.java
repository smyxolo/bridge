package Zadanie2;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class VehicleController {

    public static void main(String[] args) {
        List<IVehicle> vehicleList = Arrays.asList(new Car("Peugeot"), new Boat("Volvo"), new AirCraft("Boeing"));
        Scanner scan = new Scanner(System.in);
        boolean isWorking = true;

        while (isWorking) {
            System.out.println("Enter an integer between 1 and 4:");
            String inputLine = scan.nextLine();

            switch (inputLine) {
                case "1": {
                    for (IVehicle v : vehicleList) {
                        v.moveUp();
                    }
                    break;
                }

                case "2": {
                    for (IVehicle v : vehicleList) {
                        v.moveDown();
                    }
                    break;
                }

                case "3": {
                    for (IVehicle v : vehicleList) {
                        v.moveLeft();
                    }
                    break;
                }

                case "4": {
                    for (IVehicle v : vehicleList) {
                        v.moveRight();
                    }
                    break;
                }

                case "quit": {
                    System.exit(0);
                    break;
                }
            }
        }
    }

}
