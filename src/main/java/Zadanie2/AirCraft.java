package Zadanie2;

public class AirCraft implements IVehicle{
    private String name;

    public AirCraft(String name) {
        this.name = name;
    }

    @Override
    public void moveLeft() {
        System.out.println(name + " has flown left");

    }

    @Override
    public void moveRight() {
        System.out.println(name + " has flown right");

    }

    @Override
    public void moveUp() {
        System.out.println(name + " has flown up");

    }

    @Override
    public void moveDown() {
        System.out.println(name + " has flown down");

    }
}
