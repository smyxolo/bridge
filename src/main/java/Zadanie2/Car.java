package Zadanie2;

public class Car implements IVehicle {

    private String name;

    public Car(String name) {
        this.name = name;
    }

    @Override
    public void moveLeft() {
        System.out.println(name + " has driven left");

    }

    @Override
    public void moveRight() {
        System.out.println(name + " has driven right");

    }

    @Override
    public void moveUp() {
        System.out.println(name + " has driven up");

    }

    @Override
    public void moveDown() {
        System.out.println(name + " has driven down");

    }
}
