package Zadanie2;

public class Boat implements IVehicle {
    private String name;

    public Boat(String name) {
        this.name = name;
    }

    @Override
    public void moveLeft() {
        System.out.println(name + " has sailed left");

    }

    @Override
    public void moveRight() {
        System.out.println(name + " has sailed right");

    }

    @Override
    public void moveUp() {
        System.out.println(name + " has sailed up");

    }

    @Override
    public void moveDown() {
        System.out.println(name + " has sailed down");

    }
}
